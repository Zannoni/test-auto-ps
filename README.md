Préparation son environnement pour Windows 10

Installer Java JDK
Installer IntelliJ IDEA

Créer le projet sous IntelliJ IDEA

Ecrire le premier test Cucumber "GoogleRecherche.feature"

Lancer le premier test "AppTest.java"

Implémenter les steps "GoogleRechercheStepDefinition.java"


Installer Maven sous Windows 10

Créer Maven build pour Selenium Framework

Lancer Selenium test sur Jenkins

Deploiement sur Jenkins

Pour plus de détails, veuillez voir le doc sur Confluence.